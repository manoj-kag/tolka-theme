<section class="section-post section-post--video py-md-8 py-3">
<div class="container">
	<div class="row">

		<div class="col-12 col-md-10 offset-md-1">

			<?php if(get_sub_field("heading")) : ?>
				<h2><?php the_sub_field("heading") ?></h2>
			<?php endif ?>
		</div>

		<div class="col-12 col-md-10 offset-md-1">

			<?php if(get_sub_field("video_iframe")) : ?>
				<div class="fluid-width-video-wrapper">
					<?php the_sub_field("video_iframe") ?>
				</div>
			<?php endif ?>
			<?php if(get_sub_field("caption")) : ?>
				<p><?php the_sub_field("caption") ?></p>
			<?php endif ?>
		</div>
		</div>
	</div>
</section>

