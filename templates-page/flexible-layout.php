<?php
/**
 * Template Name: Flexible Layout
 *
 * Template for displaying a page without sidebar
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>
	<main class="site-main py-md-8 py-5" id="main" role="main">
			<?php while(the_flexible_field("flexible_content")): ?>

		<?php if(get_row_layout() == "video"): //  ?>
			<?php get_template_part( 'templates-acf/post/video' ); ?>

		<?php elseif(get_row_layout() == "text_area"): //  ?>
			<?php get_template_part( 'templates-acf/post/text-area' ); ?>

		<?php elseif(get_row_layout() == "text_two_col"): //  ?>
			<?php get_template_part( 'templates-acf/post/text-two-col' ); ?>


		<?php elseif(get_row_layout() == "image_full_width"): //  ?>
			<?php get_template_part( 'templates-acf/post/image-full-width' ); ?>

		<?php endif; // end of the acf if statement ?>

		<?php endwhile; // end of the acf loop.?>
	</main><!-- #main -->

<?php
get_footer();
