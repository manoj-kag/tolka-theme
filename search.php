<?php
/**
 * The template for displaying search results pages
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();


?>

<div class="content" id="content">
<main class="site-main py-md-8 py-5" id="main" role="main">
		<div class="container"  tabindex="-1">
			<div class="row">
				<div class="col-12">

				<?php if ( have_posts() ) : ?>

					<header class="page-header">

							<h1 class="page-title">
								<?php
								printf(
									/* translators: %s: query term */
									esc_html__( 'Search Results for: %s', 'tolka' ),
									'<span>' . get_search_query() . '</span>'
								);
								?>
							</h1>

					</header><!-- .page-header -->

					<?php /* Start the Loop */ ?>
					<?php
					while ( have_posts() ) :
						the_post();

						/*
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'templates-loop/content', 'search' );
					endwhile;
					?>

				<?php else : ?>

					<?php get_template_part( 'templates-loop/content', 'none' ); ?>

				<?php endif; ?>

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php tolka_pagination();; ?>

			<!-- Do the right sidebar check -->
			</div><!-- .col-12 -->
			</div><!-- .row -->
		</div><!-- #content -->
	</main><!-- #main -->
</div>


<?php
get_footer();
