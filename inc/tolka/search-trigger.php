<?php

// function render_search_trigger() {

// 	echo "<div id='search-trigger' class='icon-search'></div>";

// }

// function render_search_modal() {
// echo '
// <div id="search-modal">
//     <div id="search-container">
//         <button title="Close (Esc)" type="button" id="search-close">×</button>
//         <div id="search">
//         <form  action="/" method="get">
//             <input type="text" name="s" placeholder="keywords..." value="" />
// 			<button type="submit" class="no-styles position-absolute"><img src="';
// 			echo get_stylesheet_directory_uri();
// 			echo '/assets/icons/icon-search.svg" alt="Search" /></button>
//         </form>
//         </div>
//     </div>
// </div>';
// }


//Alternative using Bootstrap

function render_search_trigger() {

	echo "<button id='search-trigger' type='button' class='icon-search' data-toggle='modal' data-target='#searchModal' title='open search modal'></button>";

}


function render_search_modal() {
echo '
<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="searchModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="searchModalLabel">Search</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
				<div class="modal-body">
					<form  action="/" method="get" class="d-flex flex-column justify-content-center">
							<input class="w-100 mb-3" type="text" name="s" placeholder="keywords..." value="" />
							<button class="button" type="submit">Search</button>
						</form>
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
';
}
