<?php

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
    'page_title' => 'Tolka Options',
    'menu_title' => 'Tolka Options',
    'menu_slug' => 'tolka-options',
    'capability' => 'edit_posts',
    'redirect' => false,
    ));

    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'tolka-options',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'tolka-options',
	));

}

function acf_admin_head() {
    ?>
    <style type="text/css">
    .acf-fc-layout-handle {
        background-color: #247985 !important;
        color: white !important;
    }
    .acf-flexible-content .layout .acf-fc-layout-handle .acf-fc-layout-order {
        background-color: #ffffff !important;
    }
    </style>
    <?php
    }

add_action('acf/input/admin_head', 'acf_admin_head');
