<?php

//CPT
function custom_post_type_event() {
    $labels = array(
        'name'                => _x( 'Event', 'Post Type General Name', 'tolka' ),
        'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'tolka' ),
        'menu_name'           => __( 'Event', 'tolka' ),
        'parent_item_colon'   => __( 'Parent Event', 'tolka' ),
        'all_items'           => __( 'All Events', 'tolka' ),
        'view_item'           => __( 'View Event', 'tolka' ),
        'add_new_item'        => __( 'Add New Event', 'tolka' ),
        'add_new'             => __( 'Add New', 'tolka' ),
        'edit_item'           => __( 'Edit Event', 'tolka' ),
        'update_item'         => __( 'Update Event', 'tolka' ),
        'search_items'        => __( 'Search Event', 'tolka' ),
        'not_found'           => __( 'Not Found', 'tolka' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'tolka' ),
    );

$args = array(
	'label'                 => __( 'Event', 'tolka' ),
	'menu_icon'           => 'dashicons-art',
	'description'           => __( 'Event', 'tolka' ),
	'labels'                => $labels,
	'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'custom-fields', 'page-attributes', 'revisions' ,'category'),
	//'taxonomies' 	      => array('post_tag' , 'industry'),
	'hierarchical'          => true,
	'public'                => true,
	'show_ui'               => true,
	'show_in_menu'          => true,
	'menu_position'         => 6,
	'show_in_admin_bar'     => true,
	'show_in_nav_menus'     => true,
	'can_export'            => true,
	'has_archive'           => true,
	'exclude_from_search'   => false,
	'publicly_queryable'    => true,
	'capability_type'       => 'post',
	//'taxonomies'          => array('industry'),
	'rewrite' => array(
		'slug' => 'events'
	)
	 /*// 'rewrite' => array('slug' => '/', 'with_front' => false), IF YOU WANT TO REMOVE THE SLUG FROM THE URL */

);
    register_post_type( 'event', $args );
}

add_action( 'init', 'custom_post_type_event', 0 );

