<?php

function add_gutenbery_config() {

	add_theme_support( 'disable-custom-colors' );
	add_theme_support( 'disable-custom-gradients' );
	add_theme_support ('align-wide');

	add_theme_support(
		'editor-color-palette',
		array(
			array(
				'name'  => esc_html__( 'Black', 'tolka' ),
				'slug'  => 'black',
				'color' => theme_colours['Black'],
			),
			array(
				'name'  => esc_html__( 'White', 'tolka' ),
				'slug'  => 'white',
				'color' => theme_colours['White'],
			),
			array(
				'name'  => esc_html__( 'Primary', 'tolka' ),
				'slug'  => 'primary',
				'color' => theme_colours['Primary'],
			),
			array(
				'name'  => esc_html__( 'Secondary', 'tolka' ),
				'slug'  => 'secondary',
				'color' => theme_colours['Secondary'],
			),
			array(
				'name'  => esc_html__( 'Text', 'tolka' ),
				'slug'  => 'text',
				'color' => theme_colours['Text'],
			),



		)
	);

}

add_action( 'after_setup_theme', 'add_gutenbery_config' );


add_action('acf/init', 'my_acf_init_block_types');
function my_acf_init_block_types() {

    // Check function exists.
    if( function_exists('acf_register_block_type') ) {

        // register a testimonial block.
        acf_register_block_type(array(
            'name'              => 'testimonial',
            'title'             => __('Testimonial'),
            'description'       => __('A custom testimonial block.'),
            'render_template'   => 'templates-acf/block/testimonial.php',
            'category'          => 'tolka-theme',
			'icon' => array(
				'background' => theme_colours['Primary'],
				'foreground' => '#fff',
				'src' => 'admin-comments',
			),
			'supports' => array(
				'align' 			=> false,
				'align_text' 		=> false,
				'align_content' 	=> false,
			),
            'keywords'          => array( 'testimonial', 'quote' ),
        ));
    }
}
