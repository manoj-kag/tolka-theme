<?php
add_shortcode('share-me', 'tolka_social_sharing_buttons');

function tolka_social_sharing_buttons() {
        $content = "";
        // Get current page URL
        $tolkaURL = urlencode(get_permalink());
        // Get current page title
        $tolkaTitle = str_replace( ' ', '%20', get_the_title());

        $twitterURL = 'https://twitter.com/intent/tweet?text='.$tolkaTitle.'&amp;url='.$tolkaURL.'&amp;';
        $facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$tolkaURL;
        $linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$tolkaURL.'&amp;title='.$tolkaTitle;


        $content .= '<div class="tolka-social">';
        $content .= '<h2>Help us spread the word by sharing </h2><div class="d-flex flex-column"> <a class="social-sharing tolka-twitter" href="'. $twitterURL .'" target="_blank">Twitter<i class="fa fa-twitter-square"></i></a>';
        $content .= '<a class="social-sharing tolka-facebook" href="'.$facebookURL.'" target="_blank">Facebook<i class="fa fa-facebook-square"></i></a>';
        $content .= '<a class="social-sharing social-sharingedin" href="'.$linkedInURL.'" target="_blank">LinkedIn<i class="fa fa-linkedin-square"></i></a>';
        $content .= '</div></div>';

        echo $content;
};
