<?php
/**
 * Tolka enqueue scripts
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'tolka_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function tolka_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/assets/css/theme.min.css' );
		wp_enqueue_style( 'tolka-styles', get_template_directory_uri() . '/assets/css/theme.min.css', array(), $css_version );

		wp_enqueue_script( 'jquery' );

		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/assets/js/theme.js' );
		wp_enqueue_script( 'tolka-script', get_template_directory_uri() . '/assets/js/theme.js', array(), $js_version );
		wp_enqueue_script( 'gsap', get_template_directory_uri() . '/assets/js/gsap.min.js', array(), $js_version );
		wp_enqueue_script( 'ScrollTrigger', get_template_directory_uri() . '/assets/js/ScrollTrigger.min.js', array(), $js_version );



		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // End of if function_exists( 'tolka_scripts' ).

add_action( 'wp_enqueue_scripts', 'tolka_scripts' );


function tolka_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_stylesheet_directory_uri() . '/assets/css/custom-login-style.min.css' );
}
add_action( 'login_enqueue_scripts', 'tolka_login_stylesheet' );
