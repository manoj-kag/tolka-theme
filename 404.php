<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>
	<main class="site-main py-md-8 py-5" id="main" role="main">
		<div class="container" tabindex="-1">
			<div class="row">
				<div class="col-12">

					<section class="error-404 not-found">

						<header class="page-header">

							<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'tolka' ); ?></h1>

						</header><!-- .page-header -->

						<div class="page-content">

							<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'tolka' ); ?></p>

						</div><!-- .page-content -->

					</section><!-- .error-404 -->

					</div><!-- .col-12 -->
			</div><!-- .row -->
		</div><!-- #content -->
	</main><!-- #main -->
<?php
get_footer();
