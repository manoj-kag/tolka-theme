<?php
/**
 * Post rendering content according to caller of get_template_part
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

$categories = get_the_category();
?>

<article <?php post_class("py-5"); ?> id="post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/NewsArticle">
	<a href="<?php echo get_permalink() ?>">
	<div class="d-flex flex-column flex-md-row">
		<div class="pb-3">
			<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>
		</div>
		<div class="pl-0 pl-md-3">
			<header class="entry-header">

				<?php if ( 'post' === get_post_type() ) : ?>
					<div class="entry-meta d-flex ">
						<p class="pr-3">  <?php if ( ! empty( $categories ) ) { echo esc_html( $categories[0]->name );   }  ?> <span class="pl-3">|</span>  </p>
						<?php tolka_posted_on(); ?>
					</div><!-- .entry-meta -->
				<?php endif; ?>

				<h2 class="" itemprop="headline"> <?php the_title() ?></h2>

			</header><!-- .entry-header -->


			<div class="entry-content" itemprop="articleBody">

				<?php the_excerpt(); ?>

			</div><!-- .entry-content -->

			<footer class="entry-footer">

				<div class="button"><p class="mb-0 ">Read More</p></div>

			</footer><!-- .entry-footer -->
		</div>
	</div>
	</a>
</article><!-- #post-## -->
