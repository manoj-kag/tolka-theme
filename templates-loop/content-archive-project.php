<?php
/**
 * Single post partial template
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>




<article <?php post_class(); ?> id="post-<?php the_ID(); ?>" itemscope itemtype="https://schema.org/NewsArticle">
<a href="<?php echo get_permalink() ?>">

	<header class="entry-header">

		<?php the_title( '<h1 class="entry-title" itemprop="headline">', '</h1>' ); ?>

		<div class="entry-meta">

			<?php tolka_posted_on(); ?>

		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<?php echo get_the_post_thumbnail( $post->ID, 'large' ); ?>

	<div class="entry-content" itemprop="articleBody">

		<?php the_content(); ?>

	</div><!-- .entry-content -->

	 <!-- if you want comments -->
	<!-- <footer class="entry-footer">

		<?php /*tolka_entry_footer(); */?>

	</footer><!-- .entry-footer -->

</a>
</article><!-- #post-## -->
