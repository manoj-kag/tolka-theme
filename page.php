<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

?>

	<main class="site-main py-md-8 py-5" id="main" role="main">
		<div class="container" tabindex="-1">
			<div class="row">
				<div class="col-12">

				<?php
				while ( have_posts() ) {
					the_post();
					get_template_part( 'templates-loop/content', 'page' );

				}
				?>

				</div><!-- .col-12 -->
			</div><!-- .row -->
		</div><!-- #content -->
	</main><!-- #main -->

<?php
get_footer();
