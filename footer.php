<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


?>
<?php if(  class_exists('ACF') ) : ?>
	<?php $footer_logo = get_field("footer_logo" ,"option") ?>
	<?php $copy = get_field("copyright_notice" ,"option") ?>
<?php endif ?>

<footer id="site-footer" class="" itemscope itemtype="http://schema.org/WPFooter">

	<div id="footer-top">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div><?php wp_nav_menu(array('menu' => 'footer-navigation', )); ?></div>
				</div>
			</div>
		</div>
	</div>

	<div id="footer-bottom">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="/home"><img src="<?php echo $footer_logo['url'] ?>" alt="<?php echo $footer_logo['alt'] ?>" ></a>
				</div>
				<div class="col-md-12">
				<a class="copyright">Copyright &copy; <?php echo date("Y") ?>  <?php echo $copy ?> </a>
				 - Website built by <a href="https://tolka.io" target="_blank" title="tolka web development">Tolka</a>
				</div>
			</div>
		</div>
	</div>

	</div>

</footer><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->
<!-- RENDER SEARCH MODAL IF REQUIRED -->
<?php render_search_modal() ?>
<?php wp_footer(); ?>

</body>

</html>

