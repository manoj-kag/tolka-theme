<?php

$args = array(
	'posts_per_page' => 3,
);

$featured = new WP_Query($args); ?>


<div class="container recent-posts-news  mb-4 pb-4  mb-md-8 pb-md-4">
	<div class="row">
		<div class="col-12">
			<h2 class="mb-3">Three More News Items</h2>
		</div>
		<?php if ($featured->have_posts()): ?>

				<?php while($featured->have_posts()):

					$featured->the_post();
					$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
					$author = get_the_author();
					$date = get_the_date( 'F Y');
					?>

					<div class=" col-12 col-md-4 mb-4 mb-md-0">
						<article class="news-tile">
							<a href="<?php the_permalink(); ?>">
								<img class="img-fluid w-100 mb-4" src="<?php echo $featured_img_url ?>">
								<?php the_excerpt(); ?>
								<p class="author mb-2"> <?php echo  $author ?> </p>
								<p class="date mb-0"> <?php echo  $date ?> </p>
							</a>
						</article>
					</div>

				<?php endwhile; ?>

		<?php endif; ?>
	</div>
</div>

<?php wp_reset_postdata();
?>
