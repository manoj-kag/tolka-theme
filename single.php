<?php
/**
 * The template for displaying all single posts
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>


	<main class="site-main my-5 my-md-8" id="main" role="main">

		<?php while(the_flexible_field("content_area")): ?>

			<?php if(get_row_layout() == "video"): //  ?>
				<?php get_template_part( 'templates-acf/post/video' ); ?>

			<?php elseif(get_row_layout() == "text_area"): //  ?>
				<?php get_template_part( 'templates-acf/post/text-area' ); ?>

			<?php elseif(get_row_layout() == "text_two_col"): //  ?>
				<?php get_template_part( 'templates-acf/post/text-two-col' ); ?>


			<?php elseif(get_row_layout() == "image_full_width"): //  ?>
				<?php get_template_part( 'templates-acf/post/image-full-width' ); ?>

			<?php endif; // end of the acf if statement ?>

		<?php endwhile; // end of the acf loop.?>


		<?php if ( is_single() && 'post'  == get_post_type()) : ?>

			<?php get_template_part('templates-parts/social-shares') ?>

			<?php get_template_part('templates-parts/posts-recent-news') ?>

		<?php endif ?>

	</main><!-- #main -->

<?php
get_footer();
