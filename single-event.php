<?php
/**
 * The template for displaying all single posts
 *
 * @package tolka
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
?>

	<main class="site-main py-md-8 py-5" id="main" role="main">
		<div class="container pt-md-8 pb-md-10 py-6">
			<div class="row">
				<div class="col-12 col-md-10 offset-md-1 col-lg-8 offset-lg-2"  itemscope itemtype="https://schema.org/Event">
				<h1 class="mb-3" itemprop="name">
					<?php the_title() ?>
				</h1>
				<p class="text-uppercase mb-2" itemprop="location" itemscope itemtype="https://schema.org/Place"><?php the_field('location') ?></p>
				<p class=" text-uppercase mb-6" itemprop="startDate">
					<span class="mr-3"><?php the_field('display_time') ?></span> | <span class="ml-3"><?php the_field('display_date') ?></span>
				</p>

				<div class="mb-6 ">
					<?php the_field('description') ?>
				</div>
				<div>
					<a class="button button--white" href="<?php the_field('booking_link') ?>" itemprop="url">Make a booking</a>
				</div>
			</div>
			</div>
		</div>
	</main>

<?php get_footer();
