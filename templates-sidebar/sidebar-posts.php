<aside>
	<?php get_search_form() ?>

	<h2 class="py-4">Category</h2>

	<nav>
		<div class="">
			<ul>
				<li id="item-id"><a href="/news">All posts</a></li>
			<?php $categories = get_terms( array('taxonomy' => 'category',  'hide_empty' => 0,) );
					foreach( $categories as $category ): ?>
						<li><a href="/category/<?php echo $category->slug ?>"><?php echo $category->name ?></a></li>
			<?php endforeach ?>
			</ul>
		</div>
	</nav>

</aside>
